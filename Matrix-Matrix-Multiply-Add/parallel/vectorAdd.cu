/**
 * Copyright 1993-2015 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

/**
 * Vector addition: C = A + B.
 *
 * This sample is a very basic sample that implements element by element
 * vector addition. It is the same as the sample illustrating Chapter 2
 * of the programming guide with some additions like error checking.
 */

#include <stdio.h>
#include <cstdlib>
#include <stdlib.h>     /* srand, rand */
#include <time.h>  
// For the CUDA runtime routines (prefixed with "cuda_")
#include <cuda_runtime.h>
#include <helper_cuda.h>
/**
 * CUDA Kernel Device code
 *
 * Computes the vector addition of A and B into C. The 3 vectors have the same
 * number of elements numElements.
 */
#define BLOCK_ROW 24
#define BLOCK_COL 256
#define BLOCK_K 32
#define BLOCK_SIZE 32
#define STRIDES 6
//#define DEBUG
__global__ void
gpu_mat_mul_mat(const size_t M, const size_t R, const size_t N, float * const A, float * const B, float * const C)
{
	float Cvalue = 0;
	size_t row = blockIdx.x * blockDim.x + threadIdx.x;
	size_t col = blockIdx.y * blockDim.y + threadIdx.y;
	if (row >= M || col >= N) return;
	for (int i = 0; i < R; ++i) {
		Cvalue += A[row * R + i] * B[i * N + col];
	}
	C[row * N + col] = Cvalue;
}
__global__ void
gpu_mat_mul_mat2(const size_t M, const size_t R, const size_t N, float * const A, float * const B, float * const C)
{
	float C_tx_ty = 0;
	int num_cells = (R + BLOCK_SIZE - 1) / BLOCK_SIZE;

	int bx = blockIdx.x;
	int by = blockIdx.y;
	
	int tx = threadIdx.x;
	int ty = threadIdx.y;
	
	int row_A = bx * BLOCK_SIZE + tx;
	int col_A = ty;

	int row_B = tx;
	int col_B = by * BLOCK_SIZE + ty;

	__shared__ float As[BLOCK_SIZE][BLOCK_SIZE];
	__shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];

	for (int cell_id = 0; cell_id < num_cells; ++cell_id) {
		//load value from global memory
		if (row_A < M && col_A < R) {
			As[tx][ty] = A[row_A * R + col_A];
		} else {
			As[tx][ty] = 0.0;
		}

		if (row_B < R && col_B < N) {
			Bs[tx][ty] = B[row_B * N + col_B];
		} else {
			Bs[tx][ty] = 0.0;
		}
		__syncthreads();
		//submatrix multiplication
		for (int k = 0; k < BLOCK_SIZE; ++k) {
			C_tx_ty += As[tx][k] * Bs[k][ty];
		}
		col_A += BLOCK_SIZE;//move to next cell
		row_B += BLOCK_SIZE;
		__syncthreads();//overwrite share array until all threads are done
	}
	if (row_A < M && col_B < N) {
		C[row_A * N + col_B] += C_tx_ty;
	}
}
__global__ void
gpu_mat_mul_mat3(const size_t M, const size_t R, const size_t N, float * const A, float * const B, float * const C) {
	int bx = blockIdx.x;
	int by = blockIdx.y;
	int tx = threadIdx.x;
	int ty = threadIdx.y;
	int t = tx * BLOCK_K + ty;
	int num_iter = (R + BLOCK_K - 1) / BLOCK_K;


	//int global_row_base_A = BLOCK_ROW * bx + STRIDES * tx;
	int global_col_B = BLOCK_COL * by + 2 * t;
	float Cs[BLOCK_ROW][2];
	for (int i = 0; i < BLOCK_ROW; ++i) {
		Cs[i][0] = 0.0;
		Cs[i][1] = 0.0;
	}
	__shared__ float As[BLOCK_ROW][BLOCK_K];
	for (int iter = 0; iter < num_iter; ++iter) {
		int global_col = 32 * iter + ty;
		for (int j = 0; j < STRIDES; ++j) {
			int global_row = BLOCK_ROW * bx + STRIDES * tx + j;
			int global_index = global_row * R + global_col;
			int local_row = STRIDES * tx + j;
			As[local_row][ty] = A[global_index];
		}
		__syncthreads();

		int global_row_base_B = BLOCK_K * iter;
		
		for (int j = 0; j < BLOCK_K; ++j) {
			float Bs[2];
			int global_row_B = global_row_base_B + j;
			if (global_row_B >= R) break;
			int global_index = global_row_B * N + global_col_B;
			Bs[0] = B[global_index];
			Bs[1] = B[global_index + 1];

			for (int i = 0; i < BLOCK_ROW; ++i) {
				Cs[i][0] += As[i][j] * Bs[0];
				Cs[i][1] += As[i][j] * Bs[1];
			}
		}
		__syncthreads();
	}
	int row_C = BLOCK_ROW * bx;
	for (int i = 0; i < BLOCK_ROW; ++i,++row_C) {
		if (row_C < M) {
			if (global_col_B + 1 < N) {
				//*((float2 *)(C + row_C * N + global_col_B)) = Cs[i];
				C[row_C * N + global_col_B] =  Cs[i][0];
				C[row_C * N + global_col_B + 1] = Cs[i][1];
			} else if(global_col_B < N) {
				C[row_C * N + global_col_B] = Cs[i][0];
			}
		}
	}

}
__global__ void
gpu_mat_mul_mat4(const size_t M, const size_t R, const size_t N, float * const A, float * const B, float * const C) {
	int bx = blockIdx.x;
	int by = blockIdx.y;
	int tx = threadIdx.x;
	int ty = threadIdx.y;
	int t = tx * BLOCK_K + ty;
	int num_iter = (R + BLOCK_K - 1) / BLOCK_K;


	//int global_row_base_A = BLOCK_ROW * bx + STRIDES * tx;
	int global_col_B = BLOCK_COL * by + 2 * t;
	float Cs[BLOCK_ROW][2];
	for (int i = 0; i < BLOCK_ROW; ++i) {
		Cs[i][0] = 0.0;
		Cs[i][1] = 0.0;
	}
	__shared__ float As[BLOCK_K][BLOCK_ROW];
	for (int iter = 0; iter < num_iter; ++iter) {
		int global_col = 32 * iter + ty;
		for (int j = 0; j < STRIDES; ++j) {
			int global_row = BLOCK_ROW * bx + STRIDES * tx + j;
			int global_index = global_row * R + global_col;
			int local_row = STRIDES * tx + j;
			As[ty][local_row] = A[global_index];
		}
		__syncthreads();

		int global_row_base_B = BLOCK_K * iter;

		for (int j = 0; j < BLOCK_K; ++j) {
			float Bs[2];
			int global_row_B = global_row_base_B + j;
			if (global_row_B >= R) break;
			int global_index = global_row_B * N + global_col_B;
			Bs[0] = B[global_index];
			Bs[1] = B[global_index + 1];
			for (int i = 0; i < BLOCK_ROW; ++i) {
				Cs[i][0] += As[j][i] * Bs[0];
				Cs[i][1] += As[j][i] * Bs[1];
			}
		}
		__syncthreads();
	}
	int row_C = BLOCK_ROW * bx;
	for (int i = 0; i < BLOCK_ROW; ++i, ++row_C) {
		if (row_C < M) {
			if (global_col_B + 1 < N) {
				//*((float2 *)(C + row_C * N + global_col_B)) = Cs[i];
				C[row_C * N + global_col_B] = Cs[i][0];
				C[row_C * N + global_col_B + 1] = Cs[i][1];
			}
			else if (global_col_B < N) {
				C[row_C * N + global_col_B] = Cs[i][0];
			}
		}
	}

}
__global__ void
gpu_mat_mul_mat5(const size_t M, const size_t R, const size_t N, float * const A, float * const B, float * const C) {
	int bx = blockIdx.x;
	int by = blockIdx.y;
	int tx = threadIdx.x;
	int ty = threadIdx.y;
	int t = tx * BLOCK_K + ty;
	int num_iter = (R + BLOCK_K - 1) / BLOCK_K;


	//int global_row_base_A = BLOCK_ROW * bx + STRIDES * tx;
	int global_col_B = BLOCK_COL * by + 2 * t;
	float Cs[BLOCK_ROW][2];
	for (int i = 0; i < BLOCK_ROW; ++i) {
		Cs[i][0] = 0.0;
		Cs[i][1] = 0.0;
	}
	__shared__ float As[BLOCK_K][BLOCK_ROW];
	for (int iter = 0; iter < num_iter; ++iter) {
		int global_col = 32 * iter + ty;
		for (int j = 0; j < STRIDES; ++j) {
			int global_row = BLOCK_ROW * bx + STRIDES * tx + j;
			int global_index = global_row * R + global_col;
			int local_row = STRIDES * tx + j;
			As[ty][local_row] = A[global_index];
		}
		__syncthreads();

		int global_row_base_B = BLOCK_K * iter;

		for (int j = 0; j < BLOCK_K; ++j) {
			float Bs[2];
			int global_row_B = global_row_base_B + j;
			if (global_row_B >= R) break;
			int global_index = global_row_B * N + global_col_B;
			Bs[0] = B[global_index];
			Bs[1] = B[global_index + 1];
#pragma unroll 4
			for (int i = 0; i < BLOCK_ROW; ++i) {
				Cs[i][0] += As[j][i] * Bs[0];
				Cs[i][1] += As[j][i] * Bs[1];
			}
		}
		__syncthreads();
	}
	int row_C = BLOCK_ROW * bx;
	for (int i = 0; i < BLOCK_ROW; ++i, ++row_C) {
		if (row_C < M) {
			if (global_col_B + 1 < N) {
				//*((float2 *)(C + row_C * N + global_col_B)) = Cs[i];
				C[row_C * N + global_col_B] = Cs[i][0];
				C[row_C * N + global_col_B + 1] = Cs[i][1];
			}
			else if (global_col_B < N) {
				C[row_C * N + global_col_B] = Cs[i][0];
			}
		}
	}

}
__global__ void
gpu_mat_mul_mat6(const size_t M, const size_t R, const size_t N, float * const A, float * const B, float * const C) {
	int tx = threadIdx.x;
	int ty = threadIdx.y;
	int t = tx * BLOCK_K + ty;
	int num_iter = (R + BLOCK_K - 1) / BLOCK_K;
	const float2* B2 = (float2 *)B;
	//int global_row_base_A = BLOCK_ROW * bx + STRIDES * tx;
	int global_col_B = BLOCK_COL * blockIdx.y + 2 * t;
	bool B_aligned = R % 2 == 0;
	float2 Cs[BLOCK_ROW];
	float2 zero;
	zero.x = 0.0;
	zero.y = 0.0;
	for (int i = 0; i < BLOCK_ROW; ++i) {
		Cs[i] = zero;
	}
	__shared__ float As[BLOCK_K][BLOCK_ROW];
	for (int iter = 0; iter < num_iter; ++iter) {
		int global_col = 32 * iter + ty;
		for (int j = 0; j < STRIDES; ++j) {
			int global_row = BLOCK_ROW * blockIdx.x + STRIDES * tx + j;
			int global_index = global_row * R + global_col;
			int local_row = STRIDES * tx + j;
			As[ty][local_row] = A[global_index];
		}

		__syncthreads();

		int global_row_B = BLOCK_K * iter;
		int global_index = global_row_B * N + global_col_B;
		float2 Bs;
		for (int j = 0; j < BLOCK_K; ++j, global_index += N, global_row_B++) {

			if (global_row_B >= R) break;
			if (B_aligned) {
				Bs = B2[global_index / 2];
			}
			else {
				Bs.x = B[global_index];
				Bs.y = B[global_index + 1];
			}
			for (int i = 0; i < BLOCK_ROW; ++i) {
				Cs[i].x += As[j][i] * Bs.x;
				Cs[i].y += As[j][i] * Bs.y;
			}
		}
		__syncthreads();
	}
	int row_C = BLOCK_ROW * blockIdx.x;
	int global_index_C = row_C * N + global_col_B;
	for (int i = 0; i < BLOCK_ROW; ++i, ++row_C, global_index_C += N) {
		if (row_C < M) {
			if (global_col_B + 1 < N) {
				C[global_index_C] = Cs[i].x;
				C[global_index_C + 1] = Cs[i].y;
			}
			else if (global_col_B < N) {
				C[global_index_C] = Cs[i].x;
			}
		}
	}

}
__global__ void
gpu_mat_mul_mat7(const size_t M, const size_t R, const size_t N, float * const A, float * const B, float * const C) {
	int tx = threadIdx.x;
	int ty = threadIdx.y;
	int t = tx * BLOCK_K + ty;
	int num_iter = (R + BLOCK_K - 1) / BLOCK_K;
	const float2* B2 = (float2 *)B;
	//int global_row_base_A = BLOCK_ROW * bx + STRIDES * tx;
	int global_col_B = BLOCK_COL * blockIdx.y + 2 * t;
	bool B_aligned = R % 2 == 0;
	float2 Cs[BLOCK_ROW];
	float2 zero;
	zero.x = 0.0;
	zero.y = 0.0;
	for (int i = 0; i < BLOCK_ROW; ++i) {
		Cs[i] = zero;
	}
	__shared__ float As[BLOCK_K][BLOCK_ROW];
	for (int iter = 0; iter < num_iter; ++iter) {
		int global_col = 32 * iter + ty;
		for (int j = 0; j < STRIDES; ++j) {
			int global_row = BLOCK_ROW * blockIdx.x + STRIDES * tx + j;
			int global_index = global_row * R + global_col;
			int local_row = STRIDES * tx + j;
			As[ty][local_row] = A[global_index];
		}

		__syncthreads();

		int global_row_B = BLOCK_K * iter;
		int global_index = global_row_B * N + global_col_B;
		float2 Bs;
		for (int j = 0; j < BLOCK_K; ++j, global_index += N, global_row_B++) {

			if (global_row_B >= R) break;
			if (B_aligned) {
				Bs = B2[global_index / 2];
			}
			else {
				Bs.x = B[global_index];
				Bs.y = B[global_index + 1];
			}
			for (int i = 0; i < BLOCK_ROW; ++i) {
				Cs[i].x += As[j][i] * Bs.x;
				Cs[i].y += As[j][i] * Bs.y;
			}
		}
		__syncthreads();
	}
	int row_C = BLOCK_ROW * blockIdx.x;
	int global_index_C = row_C * N + global_col_B;
	for (int i = 0; i < BLOCK_ROW; ++i, ++row_C, global_index_C += N) {
		if (row_C < M) {
			if (global_col_B + 1 < N) {
				C[global_index_C] = Cs[i].x;
				C[global_index_C + 1] = Cs[i].y;
			}
			else if (global_col_B < N) {
				C[global_index_C] = Cs[i].x;
			}
		}
	}

}
/* host matrix multiplication*/
void cpu_mat_mul_mat(const size_t M, const size_t R, const size_t N, float * const A, float * const B, float * const C) {
	for (int row = 0; row < M; ++row) {
		for (int col = 0; col < N; ++col) {
			C[row*N + col] = 0.0;
			for (int i = 0; i < R; ++i) {
				C[row*N + col] += A[row*R + i] * B[i*N + col];
			}
		}
	}
}
float max_abs_diff(const size_t N, const float* cpuC, const float* gpuC) {
	float max_diff = 0.0;
	for (int i = 0; i < N; ++i) {
		float diff = cpuC[i] - gpuC[i];
		diff = diff < 0 ? -diff : diff;
		max_diff = diff > max_diff ? diff : max_diff;
	}
	return max_diff;
}
/**
 * Host main routine
 */
int main(int argc, char** argv)
{	
	int m = 16;
	int r = 16;
	int n = 16;
	if (argc == 4) {
		printf("m=%d,r=%d,n=%d\n", atoi(argv[1]), atoi(argv[2]), atoi(argv[3]));
		m = atoi(argv[1]);
		r = atoi(argv[2]);
		n = atoi(argv[3]);
	}
	float* hA = (float*)malloc(m * r * sizeof(float));
	float* hB = (float*)malloc(r * n * sizeof(float));
	float* hC = (float*)malloc(m * n * sizeof(float));
	float* hgC = (float*)malloc(m * n * sizeof(float));
	if (!hA || !hB || !hC || !hgC) {
		printf("error allocating memory on host\n");
		return 1;
	}
	//random initialize matrix
	srand(time(NULL));
	for (int i = 0; i < m * r; ++i) {
		hA[i] =  (rand() + 0.0) / 100.0;
	}
	for (int i = 0; i < r * n; ++i) {
		hB[i] =  (rand() + 0.0) / 100.0;
	}
	for (int i = 0; i < m * n; ++i) {
		hC[i] = 0.0;
	}

	//computing on device
	float *dA;
	float *dB;
	float *dC;
	cudaMalloc(&dA, m * r * sizeof(float));
	cudaMalloc(&dB, r * n * sizeof(float));
	cudaMalloc(&dC, m * n * sizeof(float));
	cudaMemcpy(dA, hA, m * r * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(dB, hB, r * n * sizeof(float), cudaMemcpyHostToDevice);
	dim3 grid_dim((m + BLOCK_ROW - 1)/BLOCK_ROW, (n + BLOCK_COL - 1) / BLOCK_COL);
	dim3 block_dim(BLOCK_COL / 2 / BLOCK_K, BLOCK_K);
	gpu_mat_mul_mat7 <<<grid_dim, block_dim>>> (m, r, n, dA, dB, dC);
	cudaMemcpy(hgC, dC, m * n * sizeof(float), cudaMemcpyDeviceToHost);
#ifdef DEBUG
	cpu_mat_mul_mat(m, r, n, hA, hB, hC);
	float max_diff = max_abs_diff(m * n, hC, hgC);
	printf("max_abs_diff %f\n", max_diff);
#endif // DEBUG

	/*for (int row = 0; row < m; ++row) {
		for (int col = 0; col < n; ++col) {
			int index = row * n + col;
			printf("%.2f ", hC[index]);
		}
		printf("\n");
	}
	printf("////////////////////////\n");
	for (int row = 0; row < m; ++row) {
		for (int col = 0; col < n; ++col) {
			int index = row * n + col;
			printf("%.2f ", hgC[index]);
		}
		printf("\n");
	}*/

	cudaFree(dA);
	cudaFree(dB);
	cudaFree(dC);
	free(hA);
	free(hB);
	free(hC);
	free(hgC);
    return 0;
}

